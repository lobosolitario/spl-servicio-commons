package com.splstudio.app.commons.models.entity;

public enum TipoElemento {
	LENGUAJE,
	PRODUCTO,
	PLANTILLA
}
