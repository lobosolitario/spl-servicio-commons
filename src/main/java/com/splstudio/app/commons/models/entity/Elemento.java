package com.splstudio.app.commons.models.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "elementos")
@NamedQuery(name = "Elemento.findByProyecto", query = "select e from Elemento e where e.idProyecto = ?1")
@NamedQuery(name = "Elemento.findBySelection", query = "select e from Elemento e where ((e.idProyecto = ?1)  AND (    "
		+ "(e.tipo = 0)    OR    "
		+ "((e.tipo = 1) AND (e.id = ?2)) OR"
		+ "((e.tipo = 2) AND (e.grupo = ?3))"
		+ "))")
public class Elemento implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 4791690337921018666L;

	@Id
	private String id;

	
	@Column(name = "create_at")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createAt;
	
	private String nombre;
	
	@Lob
	@Column(length = 131072)
	private String payload;
	
	private TipoElemento tipo;
	
	private String grupo;
	
	@Column(name = "idproyecto")
	private Long idProyecto;

	@ManyToMany(fetch=FetchType.LAZY)
	private List<Proyecto> proyecto;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Date getCreateAt() {
		return createAt;
	}

	public void setCreateAt(Date createAt) {
		this.createAt = createAt;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getPayload() {
		return payload;
	}

	public void setPayload(String payload) {
		this.payload = payload;
	}


	public TipoElemento getTipo() {
		return tipo;
	}

	public void setTipo(TipoElemento tipo) {
		this.tipo = tipo;
	}

	public String getGrupo() {
		return grupo;
	}

	public void setGrupo(String grupo) {
		this.grupo = grupo;
	}

	public Long getIdProyecto() {
		return idProyecto;
	}

	public void setIdProyecto(Long idProyecto) {
		this.idProyecto = idProyecto;
	}
	
}
